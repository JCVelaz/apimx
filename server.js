var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var Usuario=require('./Usuario')
var Database=require('./database');
var CONFIG = require('./config');

Database.connect();

var path = require('path');
var movimientosv2JSON=require('./movimientosv2.json');

const bcrypt=require('bcrypt');

//var bodyparser=require('body-parser');
//app.use(bodyparser.json());

app.listen(port);

var bodyParser=require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson=require("request-json");
var urlMLabRaiz="https://api.mlab.com/api/1/databases/jvelazquez/collections";
var apiKeyMLab="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;
var urlClientes="https://api.mlab.com/api/1/databases/jvelazquez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab=requestjson.createClient(urlClientes);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res) {//req=request, res=response
    //res.send('Hola mundo desde NodeJS');
    res.sendFile(path.join(__dirname, 'index.html'));//__dirname es la carpet raiz
}) //{}

app.post('/', function(req, res) {
  res.send('Hemos recibido su peticiòn post');
})

app.put('/', function(req, res) {
  res.send('Hemos recibido su peticiòn put');
})

app.delete('/', function(req, res) {
  res.send('Hemos recibido su peticiòn delete cambiada');
})

app.get('/Clientes/:idcliente',function(req, res) {//req=request, res=response
    res.send('Aquì esta el cliente nùmero: '+ req.params.idcliente);//__dirname es la carpet raiz
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');//file minuscula para json
})

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosv2JSON);
})

app.get('/v2/Movimientos/:id', function(req, res) {
  console.log(req.params.id);
  res.send(movimientosv2JSON[req.params.id]);
})

app.get('/v2/Movimientosquery',function(req,res) {
  console.log(req.query);
  res.send('Recibido: '+ req.query.nombre);
})

app.post('/v2/Movimientos', function(req,res) {
  var nuevo =req.body
  nuevo.id=movimientosv2JSON.length+1
  movimientosv2JSON.push(nuevo)
  res.send('Movimiento dado de alta con id: '+nuevo.id)
})

app.get('/Clientes',function(req,res) {
  clienteMLab.get('',function(err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

app.post('/Clientes',function(req,res) {
  clienteMLab.post('',req.body, function(err, resM,body){
    res.send(body);//no es necesario los punto y coma
  })
})

/*app.post('/login', function(req,res) {
  res.set("Access-Control-Allow-Headers","Content-Type");
  var email=req.body.email;
  var password=req.body.password;
  var query='q={"email":"'+email+'","password":"'+password+'"}';//json es con comillas simples
  console.log(query);
  clienteMLabRaiz=requestjson.createClient(urlMLabRaiz+"/Usuarios?"+apiKeyMLab+"&"+query);
  console.log(clienteMLabRaiz);

  clienteMLabRaiz.get('',function(err, resM, body) {
    if (!err) {
      if (body.length==1) {//Login ok
        res.status(200).send('Usuario logado');
      }else {
        res.status(400).send('Usuario no encontrado');
      }
    }
  })
})*/

app.post('/login', function login(req, res, next){
  console.log(req.body);
  Usuario.findOne({email:req.body.email},(err, usuarioExistente)=>{
    if(usuarioExistente){
      bcrypt.compare( req.body.password,  usuarioExistente.password, function(error ,resA){
        if(resA){
          res.send('Usuario Logado');
        }
        res.send('usuario NO logado');
      });
    }else{
      res.send('No encontrado');
    }

  })
});

app.post('/usuarioAlta',function(req, res) {
  var urlUsuarios="https://api.mlab.com/api/1/databases/jvelazquez/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var altaUsuarios=requestjson.createClient(urlUsuarios);
  bcrypt.genSalt(10, function( err, salt) {
    bcrypt.hash( req.body.password,  salt, function( err, hash) {
      req.body.password=hash;
      altaUsuarios.post('',req.body, function(err, resM,body){
        res.send(body);
        console.log("Usaurio dado de alta correctamente");
      })
    });
  });
  /*var urlUsuarios="https://api.mlab.com/api/1/databases/jvelazquez/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var altaUsuarios=requestjson.createClient(urlUsuarios);
  altaUsuarios.post('',req.body, function(err, resM,body){
    res.send(body);
    console.log("Usaurio dado de alta correctamente");
  })*/
})

app.post('/usuarioActualizar',function(req, res) {

})

var urlMovimientos="https://api.mlab.com/api/1/databases/jvelazquez/collections/clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var conMovimientos=requestjson.createClient(urlMovimientos);

app.get('/clientes',function(req, res) {
  conMovimientos.get('',function(err, resM, body) {
    if (err) {
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

app.post('/altaMovimientos',function(req,res) {
  clienteMLab.post('',req.body, function(err, resM,body){
    res.send(body);//no es necesario los punto y coma
  })
})
